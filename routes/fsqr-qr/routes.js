const {
  fsqrQrPrints,
  printerFinds,
  printerAdds,
  printerUpdates,
} = require("../../src/controller/fsqr-qr/app");

const modules = ({ router, makeExpressCallback }) => {
  // print qr route
  router.post("/print", makeExpressCallback(fsqrQrPrints));

  // find all printers
  router.get("/get", makeExpressCallback(printerFinds));

  // add printer
  router.post("/add", makeExpressCallback(printerAdds));

  // update printer
  router.patch("/update", makeExpressCallback(printerUpdates));

  return router;
};

module.exports = modules;
