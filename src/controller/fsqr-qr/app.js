const {
  printFsqrQRs,
  findPrinters,
  addPrinters,
  updatePrinters,
} = require("../../use-case/fsqr-qr/app");

// #######################
const fsqrQrPrint = require("./qr");
const printerFind = require("./find-printer");
const printerAdd = require("./add-printer");
const printerUpdate = require("./update-printer");
// #######################
const fsqrQrPrints = fsqrQrPrint({ printFsqrQRs });
const printerFinds = printerFind({ findPrinters });
const printerAdds = printerAdd({ addPrinters });
const printerUpdates = printerUpdate({ updatePrinters });
// #######################
const services = Object.freeze({
  fsqrQrPrints,
  printerFinds,
  printerAdds,
  printerUpdates,
});

module.exports = services;
module.exports = {
  fsqrQrPrints,
  printerFinds,
  printerAdds,
  printerUpdates,
};
