const fs = require("fs");
const path = require("path");

const escpos = require("../../../hoi-escpos");
escpos.USB = require("escpos-usb");
// const escpos = require('./hoi-escpos');
const { fsqrQr, makePrinter } = require("../../entity/fsqr-qr/app"); // entity
// #######################
const printFsqrQR = require("./qr");
const findPrinter = require("./find-printer");
const addPrinter = require("./add-printer");
const updatePrinter = require("./update-printer");
// #######################
const printFsqrQRs = printFsqrQR({ fs, path, escpos, fsqrQr });
const findPrinters = findPrinter({ fs, path });
const addPrinters = addPrinter({ fs, path, makePrinter });
const updatePrinters = updatePrinter({ fs, path, makePrinter });
// #######################
const services = Object.freeze({
  printFsqrQRs,
  findPrinters,
  addPrinters,
  updatePrinters,
});

module.exports = services;
module.exports = {
  printFsqrQRs,
  findPrinters,
  addPrinters,
  updatePrinters,
};
