const addPrinter = ({ fs, path, makePrinter }) => {
  return async function posts(info) {
    const data = await makePrinter(info);
    //   check if exist;
    const bool = await checkIfExist({ fs, path });
    // create json file if it doent exist
    if (!bool) await createJSON({ fs, path });

    // make object to insert
    const printer = {
      uuid: await data.getUuid(),
      ip: await data.getIp(),
    };

    // check if file is empty or not;
    // if empty just write; else append.
    const json = await readJSON({ fs, path }); // return the array
    const len = json.length; // length of array
    if (len > 0) {
      let isAppend = false;
      // check if uuid exist
      for (let i = 0; i < json.length; i++) {
        const id = json[i].uuid;
        if (printer.uuid == id) {
          isAppend = true;
          break;
        }
      }
      if (!isAppend) await appendToFile({ fs, path, printer });
      else {
        throw new Error(`UUID already exist, please try again.`);
      }
    } else {
      // json file is empty; write
      await writeToFile({ fs, path, printer });
    }

    const msg = {
      success: true,
      msg: `Save successfully.`,
    };

    return msg;
  };
};

module.exports = addPrinter;

const checkIfExist = async ({ fs, path }) => {
  const filepath = path.resolve("printers.json");
  const data = await new Promise((resolve) => {
    fs.access(filepath, fs.F_OK, (err) => {
      if (err) {
        resolve(false);
      }
      //file exists
      resolve(true);
    });
  });
  const bool = data;
  return bool;
};

const createJSON = async ({ fs, path }) => {
  const filepath = path.resolve("printers.json");
  const data = await new Promise((resolve) => {
    fs.openSync(filepath, "w");
    resolve(true);
  });
  return data;
};

const readJSON = async ({ fs, path }) => {
  const filepath = path.resolve("printers.json");
  const info = await new Promise((resolve) => {
    fs.readFile(filepath, "utf8", function (err, data) {
      if (err) throw err;
      resolve(data);
    });
  });
  const data = info ? JSON.parse(info) : []; // empty array if null
  return data;
};

const writeToFile = async ({ fs, path, printer }) => {
  const filepath = path.resolve("printers.json");
  const info = [printer];
  const data = await new Promise((resolve) => {
    fs.writeFile(filepath, JSON.stringify(info), function (err) {
      if (err) resolve(err);
      resolve(true);
    });
  });
  return data;
};

const appendToFile = async ({ fs, path, printer }) => {
  const filepath = path.resolve("printers.json");
  const info = `,${JSON.stringify(printer)}`;
  const data = await new Promise((resolve) => {
    let counter = 0; // increment when substring is null
    fs.readFile(filepath, function read(err, data) {
      if (err) resolve(err);
      let content = data.toString(); // content insde the file
      let lastChar = null;
      let pos = 0;
      while (!lastChar || lastChar == "") {
        pos = content.length - counter; // position to insert
        lastChar = content.substring(pos);
        lastChar = lastChar.trim(); // remove white spaces
        counter++;
      }

      let file = fs.openSync(filepath, "r+"); // open the file
      let bufferedText = Buffer.from(info + lastChar);
      fs.writeSync(file, bufferedText, 0, bufferedText.length, pos); // write

      fs.close(file, () => {
        console.log("closed");
      });
    });
    resolve(true);
  });
  return data;
};
