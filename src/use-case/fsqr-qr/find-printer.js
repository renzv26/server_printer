const findPrinter = ({ fs, path }) => {
  return async function selects(info) {
    const res = await readJSON({ fs, path });
    return res;
  };
};

module.exports = findPrinter;

const readJSON = async ({ fs, path }) => {
  const filepath = path.resolve("printers.json");
  const info = await new Promise((resolve) => {
    fs.readFile(filepath, "utf8", function (err, data) {
      if (err) throw err;
      resolve(data);
    });
  });
  const data = info ? JSON.parse(info) : []; // empty array if null
  return data;
};
