const printFsqrQR = ({ fs, path, escpos, fsqrQr }) => {
  return async function posts(info) {
    const data = fsqrQr(info);

    const delay = (sec) => {
      return new Promise((resolve) => setTimeout(resolve, sec));
    };

    const header = data.getHeader();
    const qr = data.getQr();
    const uuid = data.getUuid();


    const ip = await getIp({ fs, path, uuid });
    if (!ip)
      throw new Error(
        `Your device is not yet registered, please contact administrator.`
      );
    // const device = new escpos.USB("1208", "514");
    const device = new escpos.Network(ip);

    const options = { encoding: "GB18030" /* default */ };
    // encoding is optional
    const printer = new escpos.Printer(device, options);

    const stringJSON = JSON.stringify(qr);

    //await await lang later

    //todos

    var convertQR = require("qr-image");

    var qr_svg = await convertQR.image(`${stringJSON}`, { type: "png" });
    await qr_svg.pipe(require("fs").createWriteStream("i_love_qr.png"));

    var svg_string = await convertQR.imageSync(`${stringJSON}`, {
      type: "png",
    });

    await delay(5000);

    const qrImage = await new Promise((resolve) => {
      escpos.Image.load(`./i_love_qr.png`, function (image) {
        resolve(image);
      });
    });
    await device.open(function () {
      printer
        .font("b")
        .align("ct")
        .style("bu")
        .size(2, 2)
        .text(`${header.queue}`)
        .font("a")
        .align("ct")
        .style("bu")
        .size(1, 1)
        .text(`Transaction #: ${header.transaction}`)
        .text(`Item: ${header.item}`)
        .text(`Supplier Code: ${header.supplier_code}`)
        .text(`DR #: ${header.dr}`)
        .text(`${header.date}`)
        // .qrimage(stringJSON, { type: "png", mode: "dhdw", size: 3 }, function(
        //   err
        // ) {
        //   this.cut();
        //   this.close();
        // });
        .hoiImage(qrImage)
        .text("\n")
        .cut()
        .close();

      // .font("b")
      // .align("ct")
      // .style("bu")
      // .size(2, 2)
      // .text(`${header.queue}`)
      // .font("a")
      // .align("ct")
      // .style("bu")
      // .size(1, 1)
      // .text(`Transaction #: ${header.transaction}`)
      // .text(`Item: ${header.item}`)
      // .text(`Supplier Code: ${header.supplier_code}`)
      // .text(`DR #: ${header.dr}`)
      // .text(`${header.date}`)
      // // .qrimage(stringJSON, { type: "png", mode: "dhdw", size: 3 }, function(
      // //   err
      // // ) {
      // //   this.cut();
      // //   this.close();
      // // });
      // .hoiImage(qrImage)
      // .text('\n')
      // .cut()
    });

    const msg = "Printed successfully.";
    return msg;
  };
};

module.exports = printFsqrQR;

const getIp = async ({ fs, path, uuid }) => {
  let ip = null;
  const filepath = path.resolve("printers.json");
  const info = await new Promise((resolve) => {
    fs.readFile(filepath, "utf8", function (err, data) {
      if (err) throw err;
      resolve(data);
    });
  });
  const data = info ? JSON.parse(info) : []; // empty array if null

  // get ip
  for (let i = 0; i < data.length; i++) {
    const e = data[i];
    if (e.uuid == uuid) {
      ip = e.ip;
      break;
    }
  }
  return ip;
};
