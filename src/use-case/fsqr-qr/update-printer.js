const updatePrinter = ({ fs, path, makePrinter }) => {
  return async function posts(info) {
    const data = await makePrinter(info);

    //   check if exist;
    const bool = await checkIfExist({ fs, path });

    if (!bool) throw new Error(`No data saved for now.`);

    // make object to update
    const printer = {
      uuid: await data.getUuid(),
      ip: await data.getIp(),
    };

    // if empty just write; else append.
    const json = await readJSON({ fs, path }); // return the array

    const res = await findAndReplace({ fs, path, json, printer });

    if (res) {
      const msg = {
        success: true,
        msg: `Save successfully.`,
      };

      return msg;
    } else {
      throw new Error(`Error on updating, please try again.`);
    }
  };
};

module.exports = updatePrinter;

const readJSON = async ({ fs, path }) => {
  const filepath = path.resolve("printers.json");
  const info = await new Promise((resolve) => {
    fs.readFile(filepath, "utf8", function (err, data) {
      if (err) throw err;
      resolve(data);
    });
  });
  const data = info ? JSON.parse(info) : []; // empty array if null
  return data;
};

const checkIfExist = async ({ fs, path }) => {
  const filepath = path.resolve("printers.json");
  const data = await new Promise((resolve) => {
    fs.access(filepath, fs.F_OK, (err) => {
      if (err) {
        resolve(false);
      }
      //file exists
      resolve(true);
    });
  });
  const bool = data;
  return bool;
};

const findAndReplace = async ({ fs, path, json, printer }) => {
  const filepath = path.resolve("printers.json");
  let index = 0;

  //   get the index
  for (let i = 0; i < json.length; i++) {
    const id = json[i].uuid;
    if (printer.uuid == id) {
      index = i;
      break;
    }
  }

  // remove existing
  json.splice(index, 1);

  // add new
  json.push(printer);

  const data = await new Promise((resolve) => {
    fs.writeFile(filepath, JSON.stringify(json), function (err) {
      if (err) resolve(err);
      resolve(true);
    });
  });
  return data;
};
