const qr = require("./qr");
const addPrinter = require("./add-printer");
// ###############
const fsqrQr = qr({});
const makePrinter = addPrinter({});
// ###############

module.exports = { fsqrQr, makePrinter };
