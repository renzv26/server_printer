const addPrinter = ({}) => {
  return function make({ uuid, ip } = {}) {
    if (!uuid) {
      throw new Error("Please enter UUID.");
    }
    if (!ip) {
      throw new Error("Please enter IP Address.");
    }

    return Object.freeze({
      getUuid: () => uuid,
      getIp: () => ip,
    });
  };
};

module.exports = addPrinter;
